FROM alpine:latest
LABEL author="Lukas Hardiman"

ADD output/app-linux_amd64 /root/app
# ADD config/ /root
RUN chmod +x /root/app