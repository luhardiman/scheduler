CREATE DATABASE scheduler;

CREATE TABLE img(
    `ID` VARCHAR(36) PRIMARY KEY NOT NULL,
    `image` text,
    `cron` varchar(255)
);

INSERT INTO img (`ID`, `image`, `cron`) VALUES (
    'e624141d-dafe-4815-8065-645f28b25cc1',
    'apiVersion: batch/v1
    kind: Job
    metadata:
    name: pi
    spec:
    template:
        spec:
        containers:
        - name: pi
            image: perl
            command: ["perl",  "-Mbignum=bpi", "-wle", "print bpi(2000)"]
        restartPolicy: Never
    backoffLimit: 4',
    '* * * * *'
    );