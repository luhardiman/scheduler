package ctx

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func (ctx *Application) SchedulerDB() *gorm.DB {
	if ctx.schedulingDB == nil {
		conn, err := gorm.Open("mysql",
			fmt.Sprintf("%s:%s@(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local",
				"root",
				"my-test12345",
				"0.0.0.0",
				3306,
				"scheduler",
			),
		)

		if err != nil {
			fmt.Println(err)
		}

		ctx.schedulingDB = conn
	}

	return ctx.schedulingDB
}
