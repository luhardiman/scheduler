package ctx

import "github.com/jinzhu/gorm"

type Application struct {
	schedulingDB *gorm.DB
}

func NewApplication() *Application {
	return &Application{}
}
