package main

import (
	"context"
	"fmt"
	"golang/scheduler/pkg/ctx"
	"golang/scheduler/struct/db"
	"log"
	"os"
	"path/filepath"
	"strings"

	batchv1 "k8s.io/api/batch/v1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

var ApplicationContext *ctx.Application

func connectToK8s() *kubernetes.Clientset {
	home, exists := os.LookupEnv("HOME")
	if !exists {
		home = "/root"
	}

	configPath := filepath.Join(home, ".kube", "config")

	config, err := clientcmd.BuildConfigFromFlags("", configPath)
	if err != nil {
		log.Fatalln("failed to create K8s config")
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.Fatalln("Failed to create K8s clientset")
	}

	return clientset
}

func launchK8sJob(clientset *kubernetes.Clientset, configs interface{}) {
	jobs := clientset.BatchV1().Jobs("default")
	var backOffLimit int32 = 0

	jobSpec := &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "test",
			Namespace: "default",
		},
		Spec: batchv1.JobSpec{
			Template: v1.PodTemplateSpec{
				Spec: v1.PodSpec{
					Containers: []v1.Container{
						{
							Name:    "JOB_NAME",
							Image:   "IMAGE",
							Command: strings.Split("CMD", " "),
						},
					},
					RestartPolicy: v1.RestartPolicyNever,
				},
			},
			BackoffLimit: &backOffLimit,
		},
	}

	_, err := jobs.Create(context.TODO(), jobSpec, metav1.CreateOptions{})
	if err != nil {
		log.Fatalln("Failed to create K8s job.", err)
	}

	//print job details
	log.Println("Created K8s job successfully")
}

func main() {

	ApplicationContext = ctx.NewApplication()
	ApplicationContext.SchedulerDB()
	var img []db.Img
	if err := ApplicationContext.SchedulerDB().Find(&img); err != nil {
		fmt.Println(err)
		return
	}
	clientset := connectToK8s()
	for _, v := range img {
		// fmt.Println(img.Id)
		// yaml.Unmarshal([]byte(), &)
		fmt.Println(v)
		launchK8sJob(clientset, img)

	}

	// jobName := flag.String("jobname", "test-job", "The name of the job")
	// containerImage := flag.String("image", "ubuntu:latest", "Name of the container image")
	// entryCommand := flag.String("command", "ls", "The command to run inside the container")

	// flag.Parse()

}
