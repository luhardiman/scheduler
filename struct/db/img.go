package db

import "github.com/google/uuid"

type Img struct {
	Id    uuid.UUID `gorm:"type:nvarchar(100);primary_key;column:ID"`
	Image string    `gorm:"column:image"`
	Cron  string    `gorm:"column:cron"`
}

func (Img) TableName() string {
	return "img"
}
