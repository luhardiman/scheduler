package configs

type Image struct {
	APIVersion string `yaml:"apiVersion"`
	Kind       string `yaml:"kind"`
	Metadata   string `yaml:"metadata"`
}
